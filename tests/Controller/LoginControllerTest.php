<?php

namespace Drupal\bankid_oidc\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides automated tests for the bankid_oidc module.
 */
class LoginControllerTest extends BrowserTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "bankid_oidc LoginController's controller functionality",
      'description' => 'Test Unit for module bankid_oidc and controller LoginController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests bankid_oidc functionality.
   */
  public function testLoginController() {
    // Check that the basic functions of module bankid_oidc.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}

<?php


namespace Drupal\bankid_oidc;


use BankID\OAuth2\Client\Provider\BankIdProvider;
use BankID\OAuth2\Client\Token\AccessToken;
use Drupal\user\UserInterface;

interface BankIdAuthInterface {

  public const AUTH_NAMESPACE = 'bankid_oidc';

  /**
   * Returns the oAuth client for BankID.
   *
   * @return \BankID\OAuth2\Client\Provider\BankIdProvider
   */
  public function oauthClient(): BankIdProvider;

  /**
   * Handles the authentication of a user, identified by an access token.
   *
   * @param \BankID\OAuth2\Client\Token\AccessToken $accessToken
   *
   * @return \Drupal\user\UserInterface
   */
  public function authenticate(AccessToken $accessToken): UserInterface;
}

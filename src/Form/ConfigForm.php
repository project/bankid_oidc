<?php

namespace Drupal\bankid_oidc\Form;

use BankID\OAuth2\Client\Environments;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bankid_oidc.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bankid_oidc_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bankid_oidc.config');
    $form['environment'] = [
      '#type'          => 'select',
      '#title'         => $this->t('BankdID Environment'),
      '#description'   => $this->t(
        'Each BankID client is valid for a specific environment only, please select the correct one.<br> For more info visit the <a href="@url">BankdId manual</a>',
        ['@url' => 'https://confluence.bankidnorge.no/confluence/pdoidcl/developer-resources/environments']
      ),
      '#options'       => [
        Environments::ENV_PROD  => $this->t('Production'),
        Environments::ENV_STAGE => $this->t('Staging (aka "Current")'),
        Environments::ENV_TEST  => $this->t('Testing (aka "Pre-Prod")'),
      ],
      '#default_value' => $config->get('environment'),
      '#required'      => TRUE,
    ];

    $form['client_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required'      => TRUE,
    ];

    $form['client_secret'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required'      => TRUE,
    ];
    if (\Drupal::moduleHandler()->moduleExists('key')) {
      $form['client_secret']['#type'] = 'key_select';
    }
    else {
      $message = <<<EOL
<strong>Important:</strong> We highly recommend not storing the oAuth2 Client
Secret in the site configuration and rather use the <a href="@url">Key module</a>
in combination with an Environment variable.<br>
If you install the <em>Key</em> module the "Client secret" configuration will be
automatically using its functionalities.
EOL;
      $form['client_secret_suggest_key'] = [
        '#markup' => $this->t(
          $message,
          ['@url' => 'https://www.drupal.org/project/key']
        ),
      ];
    }

    $form['scopes'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Scopes'),
      '#default_value' => $config->get('scopes'),
      '#options'       => [
        'openid'      => t('OpenID') . ' (openid)',
        'profile'     => t('Profile') . ' (profile)',
        'email'       => t('Email') . ' (email)',
        'nnin_altsub' => t('National ID Alt. identifier') . ' (nnin_altsub)',
        'address'     => t('Address') . ' (address)',
        'phone'       => t('Telephone number') . ' (phone)',
        'nnin'        => t('National ID Number') . ' (nnin)',
      ],
      '#description'   => $this->t(
        'Select which scopes should be requested. For more info refer to <a href="@url">the BankID Manual</a>',
        ['@url' => 'https://confluence.bankidnorge.no/confluence/pdoidcl/technical-documentation/core-concepts/scopes-and-claims']
      ),
    ];
    // Those scopes are required for the module to work, we only display them to
    // inform the administrator that they are always enabled.
    foreach (['openid', 'profile'] as $option_name) {
      $form['scopes'][$option_name]['#disabled'] = TRUE;
      $form['scopes'][$option_name]['#attributes']['checked'] = 'checked';
    }

    /*
    TODO: Add text format handling and enable.

    $form['auth_error'] = [
      '#type'        => 'textarea',
      '#title'       => $this->t('Authentication error message'),
      '#description' => $this->t(
        "This message will be shown to the user if an error occurs during the authentication process"
      ),
      '#default_value' => $config->get('auth_error'),
    ];
    */
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('bankid_oidc.config');
    $config->set('environment', $form_state->getValue('environment'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'));

    // Ensure that we always enable the required scopes.
    $scopes = [
        'openid'  => 'openid',
        'profile' => 'profile',
      ] + $form_state->getValue('scopes');
    $config->set('scopes', $scopes);



    $config->save();
  }

}
